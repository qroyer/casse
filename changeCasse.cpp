// changeCasse.cpp
// g++ -std=c++11 -Wall -Wextra -o changeCasse.out changeCasse.cpp
#include <cctype>
#include <iostream>
using namespace std;

int main()
{
    string echo;
    getline(cin,echo);
    // change les minuscules par des majuscules et réciproquement
    for (char & c : echo)
    {
        if (islower(c))
            cout << char(toupper(c));
        else if (std::isupper(c))
            cout << char(tolower(c));
        else 
            cout << c;
    }
    return 0;
}
